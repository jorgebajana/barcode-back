'use string';

// const moment = require('moment');
var ObjectId = require('mongoose').Types.ObjectId; 

const List = require('../models/list.model');
const Imei = require('../models/imei.model');

const prueba = (req, res) => {
	res.status(200).send({message: 'Prueba'});
}

const addList = (req, res) => {
	const list = new List();
    const params = req.body;
    
    if (params.name) {
        list.name = params.name;
        list.date = new Date();

        list.save((err, listStored) => {
            if (err) return res.status(500).send({message: err});
            if (!listStored) return res.status(500).send({message: 'Error en el guardado de la lista'});
            res.status(200).send({list: listStored});
        })
    } else {
        res.status(200).send({message: 'El nombre de la lista es obligatorio'});
    }
}

const getLists = (req, res) => {
	List.find({}).exec((err, lists) => {
        if (err) return res.status(500).send({message: 'Error en el servidor'});
        if (!lists) return res.status(404).send({message: 'No hay listas'});
        res.status(200).send({lists});
    })
}

const removeList = (req, res) => {
	const listId = req.params.id;
    List.findByIdAndRemove(listId, (err, listRemoved) => {
        if (err) return res.status(500).send({message: 'Error en la petición'});
        if (!listRemoved) return res.status(404).send({message: 'No se ha podido borrar la lista'});
        // res.status(200).send({list: listRemoved});
        Imei.deleteMany({list: new ObjectId(listId)}).exec((err, imeis) => {
			if (err) return res.status(500).send({message: 'Error en el servidor'});
	        if (!imeis) return res.status(404).send({message: 'Borrados los IMEIS de la lista'});
	        res.status(200).send({imeis, list: listRemoved});
		})
    });
}

const addImei = (req, res) => {
	const imei = new Imei();
	const params = req.body;

	if (params.name) {
		imei.name = params.name;
		imei.list = params.list;

		imei.save((err, imeiStored) => {
			if (err) return res.status(500).send({message: err});
            if (!imeiStored) return res.status(500).send({message: 'Error en el guardado del IMEI'});
            res.status(200).send({imei: imeiStored});
		})
	} else {
		res.status(200).send({message: 'El numero de IMEI es incorrecto'});
	}
}

const getAllImeis = (req, res) => {
	Imei.find({}).exec((err, imeis) => {
		if (err) return res.status(500).send({message: 'Error en el servidor'});
        if (!imeis) return res.status(404).send({message: 'No hay imeis'});
        res.status(200).send({imeis});
	});
}

const getImeis = (req, res) => {
	const listId = req.params.id;

	Imei.find({list: new ObjectId(listId)}).exec((err, imeis) => {
		if (err) return res.status(500).send({message: 'Error en el servidor'});
        if (!imeis) return res.status(404).send({message: 'No hay imeis'});
        res.status(200).send({imeis});
	})
}

const removeImei = (req, res) => {
	const imeiId = req.params.id;
    Imei.findByIdAndRemove(imeiId, (err, imeiRemoved) => {
        if (err) return res.status(500).send({message: 'Error en la petición'});
        if (!imeiRemoved) return res.status(404).send({message: 'No se ha podido borrar lel IMEI'});
        res.status(200).send({imei: imeiRemoved});
    })
}

module.exports = {
	prueba,
	// Listas
	addList,
	getLists,
	removeList,
	// Imeis
	addImei,
	getAllImeis,
	getImeis,
	removeImei
}