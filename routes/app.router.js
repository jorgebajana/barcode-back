'use strict';

const express = require('express');
const AppController = require('../controllers/app.controller');

const api = express.Router();

// api.get('/prueba', AppController.prueba);

// List actions
api.post('/addList', AppController.addList);
api.get('/getLists', AppController.getLists);
api.delete('/removeList/:id', AppController.removeList);

// IMEI actions
api.post('/addImei', AppController.addImei);
api.get('/getAllImeis', AppController.getAllImeis);
api.get('/getImeis/:id', AppController.getImeis);
api.delete('/removeImei/:id', AppController.removeImei);


module.exports = api;
