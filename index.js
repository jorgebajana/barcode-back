'use strict';

const mongoose = require('mongoose')
const app = require('./app');
const PORT = process.env.PORT || 3000;

mongoose.connect('mongodb://54.38.240.40:27017/barcode?authSource=admin', { useNewUrlParser: true, user: 'barcode', pass: 'b4rc0d3APP' }, (err, res) => {
    if (err) { throw err }
    else { 
        console.log("La conexión se ha hecho correctamente") 
        app.listen(PORT, (res, req, err) => {
            console.log(`El servidor está corriendo en el puerto ${PORT}`)
        })
    }
});
