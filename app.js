'use strict';

const express = require('express');
const bodyParser = require('body-parser');

const app = express();

// Rutas
const app_routes = require('./routes/app.router');

// Middlewares de Body Parser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Configurar cabeceras y CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    res.header('Allow', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
})

// Rutas base
app.use('/api', app_routes);

app.get('/', (req, res) => {
    res.status(200).send('API REST para barcodeapp');
})

module.exports = app;