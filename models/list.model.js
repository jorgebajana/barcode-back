'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ListSchema = Schema({
    name: String,
    date: Date
});

module.exports = mongoose.model('List', ListSchema);