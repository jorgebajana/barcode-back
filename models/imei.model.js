'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ImeiSchema = Schema({
    name: Number,
    list: { type: Schema.ObjectId, ref: 'List' }
});

module.exports = mongoose.model('Imei', ImeiSchema);